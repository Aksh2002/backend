const express = require("express");
const mongoose = require("mongoose");
const Cart = require("../models/Cart");
const fetchuser = require("../middlewares/fetchuser");
const Product = require("../models/Product");
const router = express.Router();

//add to cart
router.post("/addtocart/:productId", fetchuser, async (req, res) => {
  try {
    const user = req.user;
   
    if (!user) {
      console.log("Login Kro Phle !");
      return res.status(401).json({ message: "Unauthorized user" });
    }
    console.log(user)

    const { productId } = req.params;
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({
        message: "Product not found",
      });
    }

    // Check if the product is visible or not
    if (!product.visible) {
      return res.status(403).json({
        message:
          "Product is not visible by admin you can add only visible products",
      });
    }

    let cart = await Cart.findOne({ user: user.id });

    if (!cart) {
      cart = new Cart({ user: user.id });
      await cart.save();
    }

    const existingProductIndex = cart.products.findIndex(
      (item) => item.product._id.toString() === product._id.toString()
    );

    if (existingProductIndex >= 0) {
      cart.products[existingProductIndex].quantity++;
      await cart.save();

      const updatedProduct = {
        product,
        quantity: cart.products[existingProductIndex].quantity,
      };

      return res.json({
        message: "Item quantity increased by 1",
        product: product,
        quantity: cart.products[existingProductIndex].quantity,
      });
    } else {
      cart.products.push({ product, quantity: 1 });
      await cart.save();

      return res.json({ message: "Product added to cart", product });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

// Delete product from cart route
router.delete("/deletetocart/:id", fetchuser, async (req, res) => {
  try {
    const user = req.user;
  

    if (!user) {
      return res.status(401).json({ message: "Unauthorized access" });
    }

    const { id } = req.params;

    const cart = await Cart.findOne({ user: user.id });

    if (!cart) {
      return res.status(404).json({
        message: "Cart not found",
      });
    }

    const productIndex = cart.products.findIndex(
      (item) => item.product._id.toString() === id
    );

    if (productIndex >= 0) {
      cart.products.splice(productIndex, 1);
      await cart.save();

      return res.json({
        message: "Product removed from cart",
      });
    } else {
      return res.status(404).json({
        message: "Product not found in the cart",
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

// see my cart products
router.get("/mycart", fetchuser, async (req, res) => {
  try {
    const user = req.user;

    if (!user) {
      return res.status(401).json({ message: "Unauthorized access" });
    }

    const cart = await Cart.findOne({ user: user.id }).populate(
      "products.product"
    );

    if (!cart) {
      return res.status(404).json({
        message: "Cart not found",
      });
    }

    if (cart.products.length === 0) {
      return res.status(400).json({
        message: "You have no products in your cart",
      });
    }

    //   remove extra _id field
    const cartWithoutSubdocumentId = {
      _id: cart._id,
      products: cart.products.map(({ product, quantity }) => ({
        product,
        quantity,
      })),
      __v: cart.__v,
    };

    return res.json(cartWithoutSubdocumentId);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

module.exports = router;
