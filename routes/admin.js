const express = require("express");
const router = express.Router();
const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Admin Login
router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(401).json({ message: "Invalid credentials" });
    }
    if (user.userName === "admin" && user.isAdmin && (await bcrypt.compare(password, user.password))) {
      // Generate a token for admin
      const token = jwt.sign({ userId: user._id, isAdmin: true }, process.env.JWTwebToken, { expiresIn: "24h" });

      return res.status(200).json({msg:"admin login ",user,token });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
});

module.exports = router;
