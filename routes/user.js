require("dotenv").config();
const express = require("express");
// const sharp = require('sharp')
const User = require("../models/User");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const fetchuser = require("../middlewares/fetchuser");
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const Wishlist = require("../models/Wishlist");
const { log } = require("console");
const Product = require("../models/Product");
const checkAdmin = require("../middlewares/checkadmin");

const cloudinary = require("cloudinary").v2;

const replaceSpacesWithUnderscores = (filename) => {
  return filename.replace(/\s+/g, "_");
};

const pdfStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    const formattedUserName = req.body.userName.replace(/\s+/g, "_");
    const userPath = `./public/pdfs/${formattedUserName}`;

    if (!fs.existsSync(userPath)) {
      fs.mkdirSync(userPath, { recursive: true });
    }

    cb(null, userPath);
  },
  filename: (req, file, cb) => {
    const originalFileName = path.parse(file.originalname).name;
    const safeFileName = replaceSpacesWithUnderscores(originalFileName);
    const uniqueFilename = `${safeFileName}${path.extname(file.originalname)}`;
    cb(null, uniqueFilename);
  },
});

const pdfUpload = multer({ storage: pdfStorage });

// signup route
router.post(
  "/signup",
  [
    body("email", "Enter a unique and valid Email."),
    body("password", "Minimum length of password should be 5"),
  ],
  pdfUpload.array("media", 10),
  async (req, res) => {
    const errors = validationResult(req);

    try {
      // Process uploaded PDFs
      for (const file of req.files) {
        const buffer = await sharp(file.buffer).toBuffer();

        const filePath = path.join(
          __dirname,
          `../public/pdfs/${req.body.userName}/${file.filename}`
        );

        fs.writeFileSync(filePath, buffer, (err) => {
          if (err) throw new Error(err);
        });
      }

      if (!errors.isEmpty()) {
        return res.json({ errors: errors.array() });
      }

      const formattedUserName = req.body.userName.replace(/\s+/g, "_");
      const hashedPassword = await bcrypt.hash(req.body.password, 10);

      const newUser = await User.create({
        userName: formattedUserName,
        email: req.body.email,
        password: hashedPassword,
        // Add other user properties as needed
      });

      // Add additional logic for updating user with file URLs if required

      const data = {
        user: {
          id: newUser.id,
        },
      };

      const jwtData = jwt.sign(data, process.env.JWTwebToken);
      res.status(200).json({
        message: "User created successfully",
        user: newUser,
        jwtData,
      });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Internal server error occurred");
    }
  }
);

// login route
router.post(
  "/login",
  [
    body("email", "Enter an unique and valid Email.").isEmail(),
    body("password", "Password cannot be blank").exists(),
  ],
  async (req, res) => {
    let success = false;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array() });
    }
    const { email, password } = req.body;
    try {
      let user = await User.findOne({ email });
      if (!user) {
        return res.json({ success, error: "Email or Password is wrong" });
      }

      const passwordCompare = await bcrypt.compare(password, user.password);

      if (!passwordCompare) {
        return res.json({ success, error: "Password is wrong" });
      }

      const data = {
        user: {
          id: user.id,
        },
      };
      const jwtData = jwt.sign(data, process.env.JWTwebToken);
      success = true;
      res.status(200).json({
        success: true,
        message: "User login successful",
        // user,
        jwtData,
      });
    } catch (error) {
      console.log(error.message);
      res.status(500).send("Internal Server Error");
    }
  }
);

// Update User status (accept or reject)
router.put("/updateUserStatus/:id", checkAdmin, async (req, res) => {
  try {
    console.log(req.body);
    const { id } = req.params;
    let { status } = req.body;

    //   ex: {
    //     "status": "accept"
    // }

    // Check if the 'status' property exists in the request body
    if (!status) {
      return res
        .status(400)
        .json({ error: "Missing status property in the request body" });
    }

    // Convert status to lowercase for case-insensitive matching
    status = status.toLowerCase();

    // Validate the status value (assuming only 'accept' and 'reject' are allowed)
    if (!["accept", "reject"].includes(status)) {
      return res.status(400).json({ error: "Invalid status value" });
    }

    const updatedUser = await User.findByIdAndUpdate(
      id,
      { $set: { status } },
      { new: true }
    );

    res
      .status(200)
      .json({ message: "User status updated successfully", updatedUser });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

//  see all accepted Users(All accdpted vendors)
router.get("/acceptedUsers", checkAdmin, async (req, res) => {
  try {
    const acceptedUser = await User.find({ status: "accept" });
    if (!acceptedUser) {
      return res.status(404).json({
        message: "No accepted Users found it maybe pending or rejected",
      });
    }
    res.status(200).json(acceptedUser);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

//  see all accepted Users(All rejected vendors)
router.get("/rejectedUsers", checkAdmin, async (req, res) => {
  try {
    const rejectedUser = await User.find({ status: "reject" });
    if (!rejectedUser || rejectedUser.length === 0) {
      return res.status(404).json({
        message:
          "No users found with 'reject' status. Users may be in 'pending' or 'accepted' status.",
      });
    }
    res.status(200).json(rejectedUser);
  } catch (error) {
    console.error(
      `Error occurred while fetching rejected users: ${error.message}`
    );
    res.status(500).json({
      message:
        "An internal server error occurred while fetching rejected users. Please try again later.",
    });
  }
});

//  get user details route
router.get("/getuser", fetchuser, async (req, res) => {
  try {
    // if (!req.user) {
    //   return res.status(401).json({
    //     error: "Authentication failed. Please provide a valid token.",
    //   });
    // }

    const userId = req.user.id;

    const user = await User.findById(userId).select("-password");

    if (!user) {
      return res.status(404).json({ error: "User not found." });
    }

    // Generate full paths for PDF documents
    const basePdfURL = process.env.PDF_BASE_URL || "http://localhost:8080/pdfs";
    // const taxCertificateURL = `${basePdfURL}/${user.userName}/${user.taxCertificate}`;
    // const businessID1URL = `${basePdfURL}/${user.userName}/${user.businessID1}`;
    // const businessID2URL = `${basePdfURL}/${user.userName}/${user.businessID2}`;

    const userDetailsWithPDFURLs = {
      ...user.toObject(),
      // taxCertificateURL,
      // businessID1URL,
      // businessID2URL,
    };

    res.json({
      msg: "User details fetched",
      userDetails: userDetailsWithPDFURLs,
    });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

//  get user details route for sarthak
router.get("/getuser/:id", checkAdmin, async (req, res) => {
  try {
    // if (!req.user) {
    //   return res.status(401).json({
    //     error: "Authentication failed. Please provide a valid token.",
    //   });
    // }

    const userId = req.params.id;
    const user = await User.findById(userId).select("-password");

    if (!user) {
      return res.status(404).json({ error: "User not found." });
    }

    const userDetailsWithPDFURLs = {
      ...user.toObject(),
    };

    res.json({
      msg: "User details fetched",
      userDetails: userDetailsWithPDFURLs,
    });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// See all users
router.get("/allUsers", checkAdmin, async (req, res) => {
  try {
    const allUsers = await User.find();

    if (!allUsers || allUsers.length === 0) {
      return res.status(404).json({
        message: "No users found.",
      });
    }

    // Generate full paths for PDF documents for each user
    const basePdfURL = process.env.IMAGE_BASE_URL || "http://localhost:8080/pdfs";
    const usersWithPDFURLs = allUsers.map((user) => ({
      ...user.toObject(),
      // taxCertificateURL: `${basePdfURL}/pdfs/${user.userName}/${user.taxCertificate}`,
      // businessID1URL: `${basePdfURL}/pdfs/${user.userName}/${user.businessID1}`,
      // businessID2URL: `${basePdfURL}/pdfs/${user.userName}/${user.businessID2}`,
    }));

    res.status(200).json(usersWithPDFURLs);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// delete user
router.delete("/deleteuser/:id", checkAdmin, async (req, res) => {
  try {
    // Get the user ID from the request parameters
    const userId = req.params.id;
    // Find the user by ID and delete it
    const user = await User.findByIdAndDelete(userId);
    // If the user does not exist, return an error
    if (!user) {
      return res.status(404).json({ error: "User not found." });
    }
    // If the user was deleted successfully, return a success message
    res.json({ msg: "User deleted successfully", user });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

//! Wishlist routes

// add to wishlist
router.post("/addtowishlist/:productId", fetchuser, async (req, res) => {
  const { productId } = req.params;

  const product = await Product.findById(productId);
  try {
    const user = req.user;

    if (!user) {
      return res.status(401).json({ message: "Unauthorized access" });
    }

    // Check if the product is visible to the user

    if (!product.visible) {
      return res.status(400).json({
        message: "Product is not visible you can't add it to wishlist",
      });
    }

    let wishlist = await Wishlist.findOne({ user: user.id });

    let isNewWishlist = false;

    if (!wishlist) {
      const newWishlist = new Wishlist({
        user: user.id,
        products: [{ product: productId }],
      });

      await newWishlist.save();
      // wishlist = newWishlist;
      // isNewWishlist = true;
    } else {
      const existingProductIndex = wishlist.products.findIndex(
        (item) => item.product.toString() === productId
      );

      if (existingProductIndex >= 0) {
        return res.json({ message: "Product already in wishlist" });
      }

      wishlist.products.push({ product: productId });

      await wishlist.save();

      // await Wishlist.findOneAndUpdate(
      //   { user: user._id },
      //   { $set: { products: wishlist.products } }
      // );
    }

    const updatedwishlist = await Wishlist.find().populate("products.product");

    // Simplify the response structure
    const response = {
      message: "Product added to wishlist",
      wishlist: updatedwishlist,
    };

    return res.json(response);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
});

// Remove product from wishlist
router.delete("/removefromwishlist/:productId", fetchuser, async (req, res) => {
  try {
    const user = req.user;
    // console.log(user);

    if (!user) {
      return res.status(401).json({ message: "Unauthorized access" });
    }

    const { productId } = req.params;
    const wishlist = await Wishlist.findOne({ user: user.id });

    if (!wishlist) {
      return res.status(404).json({
        message: "Wishlist not found",
      });
    }

    const productIndex = wishlist.products.findIndex(
      (item) => item.product.toString() === productId
    );

    if (productIndex >= 0) {
      wishlist.products.splice(productIndex, 1);
      await wishlist.save();
      return res.json({ message: "Product removed from wishlist", wishlist });
    } else {
      return res.status(404).json({
        message: "Product not found in the wishlist",
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

router.get("/mywishlist", fetchuser, async (req, res) => {
  try {
    const user = req.user;

    if (!user) {
      return res.status(401).json({ message: "Unauthorized access" });
    }

    const wishlist = await Wishlist.findOne({ user: user.id }).populate(
      "products.product"
    );

    if (!wishlist) {
      return res.status(404).json({
        message: "Wishlist not found",
      });
    }

    const wishlistWithoutSubdocumentId = {
      _id: wishlist._id,
      products: wishlist.products.map(({ product, addedAt }) => ({
        product,
        addedAt,
      })),
      __v: wishlist.__v,
    };

    return res.json(wishlistWithoutSubdocumentId);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

//! ADMIN purpose
router.get("/allwishlist", checkAdmin, async (req, res) => {
  try {
    const wishlist = await Wishlist.find()
      .populate("products.product")
      .populate("user");

    return res.json(wishlist);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
});

module.exports = router;
