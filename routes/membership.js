const express = require("express");
const MembershipSchema = require("../models/Membership");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const fs = require("fs");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const businessName = req.body.businessName;
        const vendorPath = `./public/MembershipFiles/${businessName}`;
        
        if (!fs.existsSync(vendorPath)) {
            fs.mkdirSync(vendorPath);
        }

        cb(null, vendorPath);
    },
    filename: (req, file, cb) => {
        let prefix = req.body.businessName;
        cb(null, prefix + file.fieldname + path.extname(file.originalname));
    }
});

const upload = multer({ storage });
router.post("/upload", upload.fields([{ name: 'businessID1', maxCount: 1 }, { name: 'businessID2', maxCount: 1 }, { name: 'taxCertificate', maxCount: 1 }]), async (req, res) => {
    try {
        const { businessName,salesTaxReseller,email,businessTaxID,phoneNumber,streetAddress1,streetAddress2,city,state,zipCode,country,businessType,primaryBuyer1,primaryBuyer2,emailUpdates,textUpdates } = req.body;
        const businessID1 = req.files['businessID1'][0].filename;
        const businessID2 = req.files['businessID2'][0].filename;
        const taxCertificate = req.files['taxCertificate'][0].filename;
        const newVendor = await MembershipSchema.create({
            businessName,
            salesTaxReseller,
            email,
            businessTaxID,
            phoneNumber,
            streetAddress1,
            streetAddress2,
            city,
            state,
            zipCode,
            country,
            businessType,
            primaryBuyer1,
            primaryBuyer2,
            emailUpdates,
            textUpdates,
            businessID1,
            businessID2,
            taxCertificate,
        });

        res.status(200).json({ message: "Details submitted successfully" });
        
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

router.get("/all", async (req, res) => {
    try {
        const allFormData = await MembershipSchema.find();
        res.status(200).json(allFormData);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

router.patch("/update-approval", async (req, res) => {
    try {
        const { id } = req.query;
        const { approved } = req.body;

        if (approved !== true && approved !== false) {
            return res.status(400).json({ message: "Invalid value for 'approved'" });
        }

        const updatedVendor = await MembershipSchema.findByIdAndUpdate(
            id,
            { $set: { approved } },
            { new: true }
        );

        if (!updatedVendor) {
            return res.status(404).json({ message: "Vendor not found" });
        }

        res.status(200).json({ message: "Approval status updated successfully" });
        
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;