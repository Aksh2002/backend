const dotenv = require("dotenv")
dotenv.config();
const express = require("express");
const Vendor = require("../models/Vendor");
const VendorSpot = require("../models/VendorSpotlight");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const fs = require("fs");

const checkAdmin = require("../middlewares/checkadmin");
const fetchuser = require("../middlewares/fetchuser");
let c = 0;
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const vendorName = req.body.vendorName.replace(/\s+/g, "_"); // Replace spaces with underscores
    const vendorPath = `./public/images/${vendorName}`;

    // Create the vendor folder if it doesn't exist
    if (!fs.existsSync(vendorPath)) {
      fs.mkdirSync(vendorPath, { recursive: true }); // Use { recursive: true } to create nested directories
    }

    cb(null, vendorPath);
  },
  filename: (req, file, cb) => {
    let prefix = "";
    if (file.fieldname === "logo") {
      prefix = "logo";
    } else if (file.fieldname === "image1") {
      prefix = "img1";
    } else if (file.fieldname === "image2") {
      prefix = "img2";
    } else if (file.fieldname === "image3") {
      prefix = "img3";
    }
    const originalFileName = path.parse(file.originalname).name; // Get the original filename without extension
    const uniqueFilename = `${originalFileName}${path.extname(
      file.originalname
    )}`;
    cb(null, uniqueFilename);
  },
});

const upload = multer({ storage });

// add vendor route
router.post("/addVendor",checkAdmin, upload.any("file"), async (req, res) => {
  try {
    const { vendorName, desc, email } = req.body;
    const formattedVendorName = vendorName.replace(/\s+/g, "_");

    // Check if a vendor with the same email already exists
    const existingVendor = await Vendor.findOne({ email });
    if (existingVendor) {
      return res
        .status(400)
        .json({ error: "Vendor with this email already exists" });
    }

    // Check if files were uploaded
    if (!req.files || req.files.length === 0) {
      return res.status(400).json({ error: "No files uploaded" });
    }

    // Images and logos
    const logo = req.files[0].filename;
    const spotlightImages = req.files.slice(1).map((file) => file.filename);

    // Create the vendor in the Vendor collection
    const newVendor = await Vendor.create({
      vendorName: formattedVendorName,
      email,
      desc,
      logo,
      status: "pending",
    });

    // Create or update the vendorSpot in the VendorSpot collection
    const newVendorSpot = await VendorSpot.updateOne(
      { vendorName: formattedVendorName },
      {
        $set: {
          vendorName: formattedVendorName,
          desc,
          image1: spotlightImages[0],
          image2: spotlightImages[1],
          image3: spotlightImages[2],
        },
      },
      { upsert: true, new: true } // Use upsert to insert a new document if it doesn't exist
    );

    const baseImageUrl = process.env.IMAGE_BASE_URL;

    console.log("Base Image URL:", baseImageUrl);
    console.log("Formatted Vendor Name:", formattedVendorName);

    const imagesArray = spotlightImages.map(
      (filename) => `${baseImageUrl}/images/${vendorName}/${filename}`
    );

    console.log("Images Array:", imagesArray);

    res.status(200).json({
      message: "Vendor added successfully with spotlights",
      newVendor,
      imagesArray,
    });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// Delete vendor and spotlights
router.delete("/deleteVendor/:id", checkAdmin, async (req, res) => {
  try {
    const { id } = req.params;

    // Find the vendor by ID
    const vendorToDelete = await Vendor.findById(id);

    if (!vendorToDelete) {
      return res.status(404).json({ message: "Vendor not found" });
    }

    // Delete the vendor from the Vendor collection
    await Vendor.deleteOne({ _id: id });

    // Delete the vendor spotlight from the VendorSpot collection
    await VendorSpot.deleteOne({ vendorName: vendorToDelete.vendorName });

    res.status(200).json({
      message: "Vendor and spotlights deleted successfully",
      deletedVendor: vendorToDelete,
    });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// see all vendors
router.get("/allVendors", async (req, res) => {
  
  try {
    const vendors = await Vendor.find();
    const vendorsWithImageURLs = await Promise.all(
      vendors.map(async (vendor) => {
        const vendorSpotlight = await VendorSpot.findOne({
          vendorName: vendor.vendorName,
        });

        const baseImageURL = process.env.IMAGE_BASE_URL || "http://localhost:8080";

        return {
          id: vendor._id,
          vendorName: vendor.vendorName,
          email: vendor.email,
          desc: vendor.desc,
          status: vendor.status,
          logo: `${baseImageURL}/images/${vendor.vendorName}/${vendor.logo}`,
          spotlights: [
            `${baseImageURL}/images/${vendor.vendorName}/${vendorSpotlight.image1}`,
            `${baseImageURL}/images/${vendor.vendorName}/${vendorSpotlight.image2}`,
            `${baseImageURL}/images/${vendor.vendorName}/${vendorSpotlight.image3}`,
          ],
        };
      })
    );
    res.status(200).json(vendorsWithImageURLs);
  } 
  
  catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// see spotlight by name
router.post("/vendorSpotlight", async (req, res) => {
  try {
    const { vendorName } = req.body; // Assuming vendorName is sent in the request body
    const vendorSpotlight = await VendorSpot.findOne({ vendorName });
    if (!vendorSpotlight) {
      return res.status(404).json({ message: "Vendor not found" });
    }
    const vendorSpotlightWithImageURLs = {
      vendorName: vendorSpotlight.vendorName,
      content: vendorSpotlight.content,
      image1URL: `/images/${vendorSpotlight.vendorName}/${vendorSpotlight.image1}`,
      image2URL: `/images/${vendorSpotlight.vendorName}/${vendorSpotlight.image2}`,
      image3URL: `/images/${vendorSpotlight.vendorName}/${vendorSpotlight.image3}`,
    };
    res.status(200).json(vendorSpotlightWithImageURLs);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// Get vendor details by ID
router.get("/vendorDetails/:id", fetchuser, checkAdmin, async (req, res) => {
  try {
    const { id } = req.params;
    const baseImageURL = process.env.IMAGE_BASE_URL || "http://localhost:8080";

    // Find the vendor by ID
    const vendor = await Vendor.findById(id);

    if (!vendor) {
      return res.status(404).json({ message: "Vendor not found" });
    }

    // Find the vendor spotlight by vendorName
    const vendorSpotlight = await VendorSpot.findOne({
      vendorName: vendor.vendorName,
    });

    // Prepare and send the vendor details response
    const vendorDetails = {
      id: vendor._id,
      vendorName: vendor.vendorName,
      email: vendor.email,
      desc: vendor.desc,
      status: vendor.status,
      // logoURL: `/images/${vendor.vendorName}/${vendor.logo}`,
      logoURL: `${baseImageURL}/images/${vendor.vendorName}/${vendor.logo}`,
      spotlights: [
        `${baseImageURL}/images/${vendor.vendorName}/${vendorSpotlight.image1}`,
        `${baseImageURL}/images/${vendor.vendorName}/${vendorSpotlight.image2}`,
        `${baseImageURL}/images/${vendor.vendorName}/${vendorSpotlight.image3}`,
      ],
    };

    res.status(200).json({
      message: `Vendor details of${vendor.vendorName}`,
      details: vendorDetails,
    });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

module.exports = router;
