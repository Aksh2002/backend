// Import necessary modules and models
const express = require("express");
const router = express.Router();
const Order = require("../models/Order");
const Cart = require("../models/Cart");
const User = require("../models/User");
const Product = require("../models/Product"); // Import your Product model
const fetchuser = require("../middlewares/fetchuser");
const bcrypt = require("bcryptjs");
const checkAdmin = require("../middlewares/checkadmin");
const nodemailer = require("nodemailer");
const ejs = require("ejs"); // Add this line to import EJS
const path = require("path");
const crypto = require("crypto");
const { encrypt, decrypt } = require("../middlewares/enc");

// Route to place an order
router.post("/place-order", fetchuser, async (req, res) => {
  try {
    const { cartId, paymentDetails, contactDetails } = req.body;
    const userId = req.user.id;
    console.log(userId)
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    const cart = await Cart.findById(cartId).populate({
      path: "products",
      populate: {
        path: "product",
        model: "Product",
      },
    });

    if (!cart) {
      return res.status(404).json({ error: "Cart not found" });
    }

    const encryptedCardNumber = encrypt(paymentDetails.cardNumber);

    // Create a new order
    const order = new Order({
      user: {
        _id: user._id,
        email: user.email,
        userName: user.userName,
        businessName: user.businessName,
        streetAddress1: user.streetAddress1,
        streetAddress2: user.streetAddress2,
      },
      cart: cartId,
      contactDetails,
      paymentDetails: {
        cardNumber: encryptedCardNumber,
        nameOnCard: paymentDetails.nameOnCard,
        securityCode: paymentDetails.securityCode,
        expirationDate: paymentDetails.expirationDate,
      },
      cartproducts: cart.products.map((cartProduct) => ({
        productid: cartProduct.product._id,
        image:cartProduct.product.image,
        quantity: cartProduct.quantity,
        name: cartProduct.product.name,
        description: cartProduct.product.description,
        price: cartProduct.product.price,
        category: cartProduct.product.category,
        barcode: String(cartProduct.product.barcode),
        firstTier: cartProduct.product.firstTier,
        listPrice: cartProduct.product.listPrice,
        vendorName: cartProduct.product.vendorName,
      })),
    });

    // Save the order
    const savedOrder = await order.save();

    // clear cart after place order
    await Cart.findByIdAndUpdate(cartId, { $set: { products: [] } });

    // send mail after placing order using nodemailer
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.EMAIL_ID,
        pass: process.env.EMAIL_PASSWORD,
      },
      authMethod: 'PLAIN',
    });

    const templatePath = path.join(__dirname, "../public/emailtemp.ejs");
    const htmlTemplate = await ejs.renderFile(templatePath, {
      userName: savedOrder.user.userName,
      orderId: savedOrder._id,
      orderDate: savedOrder.orderDate.toLocaleDateString(),
    });

    const mailOptions = {
      from: process.env.EMAIL_ID,
      to: savedOrder.contactDetails.email,
      subject: `Thank you ${savedOrder.user.userName} for your order on TCW!`,
      html: htmlTemplate,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error(error);
      } else {
        console.log("Email sent");
      }
    });

    res
      .status(201)
      .json({ success: true, orderId: savedOrder._id, order: savedOrder });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// see my Orders
router.get("/myorders", fetchuser, async (req, res) => {
  try {
    const userId = req.user.id;
    // Find all orders for the user
    const orders = await Order.find({ "user._id": userId }).populate({
      path: "cart",
      populate: {
        path: "products.product",
        model: "Product",
      },
    });

    if (!orders || orders.length === 0) {
      return res.status(404).json({ error: "No orders found for the user" });
    }

    res.status(200).json({
      success: true,
      orders: orders.map((order) => ({
        Order_id: order._id,
        user: order.user,
        cart: order.cart,
        contactDetails: order.contactDetails,
        paymentDetails: order.paymentDetails,
        status: order.status,
        orderDate: order.orderDate,
        trackingId: order.trackingId,
      })),
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

//! ADmin can show all orders of user

router.get("/allorders", checkAdmin, async (req, res) => {
  try {
    // Find all orders
    const orders = await Order.find().populate({
      path: "cartproducts.product",
      model: "Product",
    });

    if (!orders || orders.length === 0) {
      return res.status(404).json({ error: "No orders found" });
    }

    res.status(200).json({
      success: true,
      orders: orders.map((order) => ({
        Order_id: order._id,
        user: order.user,
        cart: order.cart,
        contactDetails: order.contactDetails,
        paymentDetails: order.paymentDetails,
        cartproducts: order.cartproducts, // Include cart products directly

        status: order.status,
        orderDate: order.orderDate,
      })),
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// decode card number of order
router.get("/admin/decode-card/:orderId", checkAdmin, async (req, res) => {
  try {
    const orderId = req.params.orderId;

    // Find the order by orderId
    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ error: "Order not found" });
    }

    const encryptedCardNumber = order.paymentDetails.cardNumber;
    const decryptedCardNumber = decrypt(encryptedCardNumber);

    res.status(200).json({
      msg: `Card number decoded successfully for order ${orderId}`,
      success: true,
      orderId: order._id,
      decryptedCardNumber: decryptedCardNumber,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});




// Export the router
module.exports = router;
