const express = require("express");
require("dotenv").config();
const Product = require("../models/Product");
const router = express.Router();
// const upload = require("../middlewares/multer");
const multer = require("multer");
const path = require("path");
// Import the Cloudinary SDK
const fs = require("fs");
const csvparser = require("csv-parser");
const checkAdmin = require("../middlewares/checkadmin");
const fetchuser = require("../middlewares/fetchuser");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // Specify the destination path for saving the image
    const productimgpath = "./public/products";

    // Ensure the folder exists
    if (!fs.existsSync(productimgpath)) {
      fs.mkdirSync(productimgpath, { recursive: true });
    }

    // Callback with the destination path
    cb(null, productimgpath);
  },
  filename: function (req, file, cb) {
    const formattedFilename = file.originalname.replace(/\s+/g, "_");
    cb(null, formattedFilename);
  },
});
const upload = multer({ storage: storage });

// add product to database using multer middleware and cloudinary ✅
router.post(
  "/addproduct",
  checkAdmin,
  upload.single("image"),
  async (req, res) => {
    try {
      // console.log("Request:", req);
      // console.log("Request Body:", req.body);
      // console.log("Request Files:", req.file);

      const {
        itemid,
        name,
        description,
        price,
        category,
        status,
        vendorName,
        barcode,
        firstTier,
        listPrice,
      } = req.body;

      // Validate required fields
      if (!itemid)
        return res.status(400).json({ error: "Item ID is required" });
      if (!name)
        return res.status(400).json({ error: "Product name is required" });
      if (!description)
        return res
          .status(400)
          .json({ error: "Product description is required" });
      if (!price)
        return res.status(400).json({ error: "Product price is required" });
      if (!category)
        return res.status(400).json({ error: "Product category is required" });
      if (!barcode)
        return res.status(400).json({ error: "Product barcode is required" });
      if (!firstTier)
        return res.status(400).json({ error: "First tier is required" });
      if (!listPrice)
        return res.status(400).json({ error: "List price is required" });

      // Check if itemid already exists
      const existingProduct = await Product.findOne({ itemid });
      if (existingProduct) {
        return res.status(400).json({
          error: `Product with item ID ${itemid} already exists. Please create a unique item ID.`,
        });
      }
      // Check if barcode already exists
      const existingProductBarcode = await Product.findOne({ barcode });
      if (existingProductBarcode) {
        return res.status(400).json({
          error: `Product with barcode ${barcode} already exists. Please use a unique barcode.`,
        });
      }

      if (!req.file) {
        // Use req.file instead of req.files
        return res
          .status(400)
          .json({ error: "Please upload an image of Product" });
      }

      const formattedFilename = req.file.originalname.replace(/\s+/g, "_");

      const baseImageUrl = process.env.IMAGE_BASE_URL;

      const newProduct = new Product({
        itemid,
        name,
        description,
        price,
        category,
        image: `${baseImageUrl}/products/${formattedFilename}`, // Use local path
        status,
        vendorName,
        barcode,
        firstTier,
        listPrice,
      });
      const savedProduct = await newProduct.save();
      res
        .status(201)
        .json({ message: "Product added successfully", product: savedProduct });
    } catch (error) {
      console.error("Error in adding Product:", error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
);

// see all products from database ✅
router.get("/allproducts", async (req, res) => {
  try {
    const products = await Product.find();
    res.status(200).json(products);
  } catch (error) {
    console.error("Error in fetching products:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// see product by category using query parameter ✅
router.get("/productsbycategory", fetchuser, checkAdmin, async (req, res) => {
  try {
    let { category } = req.query;

    // Create a case-insensitive regex pattern for partial matching
    const regexPattern = new RegExp(category, "i");

    const products = await Product.find({ category: regexPattern });

    if (products.length === 0) {
      return res.status(404).json({
        message: `No products found in the category: ${category}. Please check the category name and try again.`,
      });
    }

    res.status(200).json(products);
  } catch (error) {
    console.error("Error in fetching products by category:", error);
    res.status(500).json({
      error:
        "Internal Server Error. Please try again later or contact support if the issue persists.",
    });
  }
});

// update product by id
router.put(
  "/updateproduct/:id",
  checkAdmin,
  upload.single("image"),
  async (req, res) => {
    try {
      const { id } = req.params;
      const {
        itemid,
        name,
        description,
        price,
        category,
        status,
        vendorName,
        barcode,
        firstTier,
        listPrice,
      } = req.body;

      if (!id) {
        return res.status(400).json({ error: "ID is required" });
      }

      const product = await Product.findById(id);

      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }

      product.itemid = itemid || product.itemid;
      product.name = name || product.name;
      product.description = description || product.description;
      product.price = price || product.price;
      product.category = category || product.category;
      product.status = status || product.status;
      product.vendorName = vendorName || product.vendorName;
      product.barcode = String(barcode) || String(product.barcode);
      product.firstTier = firstTier || product.firstTier;
      product.listPrice = listPrice || product.listPrice;

      if (req.file) {
        const formattedFilename = req.file.originalname.replace(/\s+/g, "_");
        const imagePath = path.join("./public/products", formattedFilename);
        fs.renameSync(req.file.path, imagePath);
        product.image = `${process.env.IMAGE_BASE_URL}/products/${formattedFilename}`;
      }

      const updatedProduct = await product.save();
      res.status(200).json(updatedProduct);
    } catch (error) {
      console.error("Error in updating product:", error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
);

// delete product
router.delete("/deleteproduct/:id", checkAdmin, async (req, res) => {
  try {
    const { id } = req.params;
    if (!id) {
      return res.status(400).json({ error: "ID is required" });
    }

    // Find product by ID
    const product = await Product.findById(id);

    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }
    // // Delete image from Cloudinary
    // const publicId = product.image
    //   .split("/")
    //   .pop()
    //   .replace(/\.[^/.]+$/, "");
    // await cloudinary.uploader.destroy(`TWC/${publicId}`);

    // // Delete product from MongoDB
    // await Product.findByIdAndDelete(id);

    // Instead of deleting, update the visibility to false
    product.visible = false;
    await product.save();

    res.status(200).json({
      success: true,
      message: "Product deleted successfully",
      deleted: product,
    });
  } catch (error) {
    console.error("Error in deleting product:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// change status of product available or outofstock
router.put("/changestatus/:id", checkAdmin, async (req, res) => {
  try {
    const { id } = req.params;
    const { status } = req.body;

    if (!id || !status) {
      return res.status(400).json({ error: "ID and status are required" });
    }

    const product = await Product.findById(id);

    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }

    // enum: ["available", "outofstock"],
    // default: "available",

    product.status = status;
    const updatedProduct = await product.save();

    res.status(200).json(updatedProduct);
  } catch (error) {
    console.error("Error in changing product status:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.post("/productSpotlight", async (req, res) => {
  try {
    const { productIdentity } = req.body;

    // Assuming vendorName is sent in the request body
    const productSpotlight = await Product.findOne({ _id: productIdentity });

    if (!productSpotlight) {
      return res.status(404).json({ message: "Product not found" });
    }

    const productSpotlightWithImageURLs = {
      productName: productSpotlight.name,
      productDescription: productSpotlight.description,
      productListPrice: productSpotlight.listPrice,
      image1URL: `${productSpotlight.image}`,
      productID: productSpotlight._id,
    };
    res.status(200).json(productSpotlightWithImageURLs);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// change visibility
router.put("/changevisiblity/:productId", checkAdmin, async (req, res) => {
  try {
    const { productId } = req.params;
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }

    product.visible = !product.visible;

    await product.save();

    res.status(200).json({
      message: `${product.name} visibility ${product.visible} done`,
      product,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

const storageCSV = multer.diskStorage({
  destination: function (req, file, cb) {
    const productimgpath = "./public/csvproducts";
    if (!fs.existsSync(productimgpath)) {
      fs.mkdirSync(productimgpath, { recursive: true });
    }
    cb(null, productimgpath);
  },
  filename: function (req, file, cb) {
    cb(null, file?.originalname);
  },
});

const uploadCSV = multer({ storage: storageCSV });

//! CSV UPLOAD
// const CSV_IMAGE_FOLDER_PATH_LOCAL = process.env.CSV_IMAGE_FOLDER_PATH_LOCAL;

// router.post("/uploadcsv", checkAdmin, uploadCSV.single("csv"), async (req, res) => {
//   try {
//     const file = req.file;
//     console.log("File:", file);

//     const stream = fs.createReadStream(file.path);
//     const products = [];

//     stream.pipe(csvparser({}))
//       .on("data", async (data) => {
//         console.log("Processing data:", data);
//         const itemid = data.itemid;

//         // Use CSV_IMAGE_FOLDER_PATH_LOCAL as the base path
//         const absoluteImagePath = path.join(CSV_IMAGE_FOLDER_PATH_LOCAL, `${itemid}.JPG`);

//         if (fs.existsSync(absoluteImagePath)) {
//           // Use server path for copying
//           const serverImagePath = path.join(__dirname, "..", "public", "csvproducts");
//           const newImagePath = path.join(serverImagePath, `${itemid}.JPG`);

//           console.log("Absolute Image Path:", absoluteImagePath);
//           console.log("New Image Path:", newImagePath);

//           try {
//             // Copy the image from local to server
//             fs.copyFileSync(absoluteImagePath, newImagePath);

//             const baseImageURL = process.env.IMAGE_BASE_URL;
//             data.image = `${baseImageURL}/csvproducts/${itemid}.JPG`;

//             products.push(data);
//           } catch (copyError) {
//             console.error(`Error copying image for product with Item ID ${itemid}:`, copyError);
//           }
//         } else {
//           console.error(`Image file not found for product with Item ID: ${itemid}`);
//         }
//       })
//       .on("end", async () => {
//         try {
//           // Save products array to the database
//           const savedProducts = await Product.insertMany(products);
//           console.log("Saved products:", savedProducts);

//           fs.unlinkSync(file.path);

//           res.status(201).json({
//             message: "Products uploaded successfully using CSV",
//             products: savedProducts,
//           });
//         } catch (error) {
//           console.error("Error saving products to the database:", error);
//           res.status(500).json({
//             error: "Internal Server Error - Error saving products to the database",
//           });
//         }
//       });
//   } catch (error) {
//     console.error("Error in uploading CSV:", error);
//     res.status(500).json({ error: "Internal Server Error" });
//   }
// });


const CSV_IMAGE_FOLDER_PATH_LOCAL = process.env.CSV_IMAGE_FOLDER_PATH_LOCAL || "D:/downloads/products";


router.post("/uploadcsv", checkAdmin, uploadCSV.single("csv"), async (req, res) => {
  try {
    const file = req.file;
    console.log("File:", file);

    const products = [];

    const stream = fs.createReadStream(file.path);
    stream.pipe(csvparser({}))
      .on("data", async (data) => {
        try {
          console.log("Processing data:", data);
          const itemid = data.itemid;
          // Use CSV_IMAGE_FOLDER_PATH_LOCAL as the base path
          const absoluteImagePath = path.join(CSV_IMAGE_FOLDER_PATH_LOCAL, `${itemid}.JPG`);
          if (fs.existsSync(absoluteImagePath)) {
            // Use server path for copying
            const serverImagePath = path.join(__dirname, "..", "public", "csvproducts");
            const newImagePath = path.join(serverImagePath, `${itemid}.JPG`);
            console.log("Absolute Image Path:", absoluteImagePath);
            console.log("New Image Path:", newImagePath);

            // Copy the image from local to server
            fs.copyFileSync(absoluteImagePath, newImagePath);

            const serverImageURL = `${process.env.HOSTED_SERVER}/csvproducts/${itemid}.JPG`;

            // Check if the image file exists on the server
            if (fs.existsSync(newImagePath)) {
              console.log(`Image found for product with Item ID: ${itemid}`);
              data.image = serverImageURL;
              products.push(data);
            } else {
              console.error(`Image file not found on the server for product with Item ID: ${itemid}`);
            }
          } else {
            console.error(`Image file not found locally for product with Item ID: ${itemid}`);
          }
        } catch (error) {
          console.error(`Error processing data for item ID ${itemid}:`, error);
        }
      })
      .on("end", async () => {
        try {
          // Save products array to the database
          if (products.length > 0) {
            const savedProducts = await Product.insertMany(products);
            console.log("Saved products:", savedProducts);
            fs.unlinkSync(file.path);
            res.status(201).json({
              message: "Products uploaded successfully using CSV",
              products: savedProducts,
            });
          } else {
            console.log("No products found in the CSV");
            fs.unlinkSync(file.path);
            res.status(200).json({
              message: "No products found in the CSV",
              products: [],
            });
          }
        } catch (error) {
          console.error("Error saving products to the database:", error);
          res.status(500).json({
            error: "Internal Server Error - Error saving products to the database",
          });
        }
      });
  } catch (error) {
    console.error("Error in uploading CSV:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
