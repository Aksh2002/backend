const jwt = require("jsonwebtoken");

const fetchuser = (req, res, next) => {
    const token =  req.header("token");
    try {
        if (!token) {
            throw new Error("Please authenticate using a valid token");
        }

        const data = jwt.verify(token, process.env.JWTwebToken);
        req.user = data.user;
        next();
    } catch (error) {
        res.status(401).send({ error: error.message });
    }
};

module.exports = fetchuser;
