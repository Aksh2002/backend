const express = require("express");
const app = express();
require("dotenv").config();
const connectDB = require("./db");
const port = process.env.PORT || 8080;
const host = process.env.HOST || '0.0.0.0';
const cors = require("cors");
cors({ all: "*" });
const bcrypt = require("bcrypt");
// routes import
const user = require("./routes/user");
const vendor = require("./routes/vendor");
const cart = require("./routes/cart");
const product = require("./routes/product");
const order = require("./routes/order");
const admin = require("./routes/admin");
const membership = require("./routes/membership");
// Connect to MongoDB
async function startServer() {
  try {
    await connectDB();
    console.log("MongoDB Connected Cloud wala 🔥🔥🔥");
    // Allow only specific origin
    // origin: "https://xcwo0wo.srv-01.purezzatechnologies.com/",
    // Configure CORS
    app.use(cors({ origin: "*" }));
    app.use(express.json());
    app.use(express.static("public"));
    app.use('/images', express.static('public/images'));



    // console.log("IMAGE_BASE_URL:", process.env.IMAGE_BASE_URL);
    


    // routes
    app.use("/api/user", user); //
    app.use("/api/product", product); //
    app.use("/api/vendor", vendor); //
    app.use("/api/cart", cart); //
    app.use("/api/order", order); //
    app.use("/api/admin", admin); //
    app.use("/api/membership", membership);
    app.get("/", (req, res) => res.send("Hello World! from server TCW "));
    // Error handling middleware
    app.use((err, req, res, next) => {
      console.error(err.stack);
      res.status(500).send(`Something went wrong!${err}`);
    });
    const hostedServer = process.env.HOSTED_SERVER;
    const serverUrl = hostedServer ? hostedServer : `http://localhost:${port}`;
    app.listen(port,host, () =>
      console.log(`Server running at ${serverUrl} 💀`)
    );
  } catch (error) {
    console.error("Error connecting to MongoDB:", error.message);
  }
}
startServer();