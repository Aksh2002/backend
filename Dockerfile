# Use the official Node.js 20.10.0 image as a base
FROM node:20.10.0-alpine

# Set the working directory to /app
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose port 8080 internally
EXPOSE 8080

# Define the command to start the application
CMD ["node", "index.js"]