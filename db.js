const mongoose = require('mongoose');
const URL = process.env.MONGO_URL;

module.exports = async () => {
  try {
    await mongoose.connect(URL);

    
  } catch (error) {
    console.error("Could not connect to the database:", error.message);
  }
};
