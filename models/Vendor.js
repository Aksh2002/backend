const mongoose = require('mongoose');
const VendorSchema = new mongoose.Schema({
    vendorName: { type: String, required: true },
    email: { type: String, required: true },
    status: { type: String, default: 'pending' },
    desc: { type: String, required: true },
    logo: { type: String, required: true },
});
const Vendor = mongoose.model('Vendor', VendorSchema);
module.exports = Vendor;