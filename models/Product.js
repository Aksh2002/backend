const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
  {
    image: {
      type: String,
    },
    itemid: {
      type: String,
      required: true,
      unique: true,
    },
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      // required: true,
    },
    category: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ["Available", "Outofstock"],
      default: "Available",
    },
    visible: {
      type: Boolean,
      default: true,
    },
    vendorName: {
      type: String,
    },
    barcode: {
      type: String,
    },
    firstTier: {
      type: Number,
    },
    listPrice: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

const Product = mongoose.model("Product", ProductSchema);

module.exports = Product;
