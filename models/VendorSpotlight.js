const mongoose = require('mongoose');
const VendorSpotSchema = new mongoose.Schema({
    vendorName: { type: String, required: true },
    desc: { type: String, required: true },
    image1: { type: String, required: true  },
    image2: { type: String, required: true  },
    image3: { type: String, required: true  },
});
const VendorSpolight = mongoose.model('VendorSpotlight', VendorSpotSchema);
module.exports = VendorSpolight;