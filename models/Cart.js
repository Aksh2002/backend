const mongoose = require("mongoose");

const cartschema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    products: [
        {
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product"
            },
            quantity: {
                type: Number,
                default: 1
            },
        }
    ]
});

const Cart = mongoose.model("Cart", cartschema);

module.exports = Cart;
