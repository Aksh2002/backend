const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  user: {
    _id: String,
    userName: String,
    email: String,
    businessName: String,
    phoneNumber: String,
    streetAddress1: String,
    streetAddress2: String,
    city: String,
    state: String,
    zip: String,
    country: String,
  },
  cart: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Cart",
    required: true,
  },
  contactDetails: {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      trim: true,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    streetAddress: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    state: {
      type: String,
      required: true,
    },
    zip: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
  },
  paymentDetails: {
    cardNumber: {
      type: String,
      required: true,
    },
    nameOnCard: {
      type: String,
      required: true,
    },
    securityCode: {
      type: String,
      required: true,
    },
    expirationDate: {
      type: String,
      required: true,
    },
  },
  cartproducts: [
    {
      product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
      },
      quantity: Number,
      name: String,
      image: String,
      description: String,
      price: Number,
      category: String,
      barcode: String,
      vendorName: String,
      firstTier: Number,
      listPrice: Number,
    },
  ],
  status: {
    type: String,
    default: "Pending",
  },
  orderDate: {
    type: Date,
    default: Date.now,
  },
  trackingId: {
    type: String,
    default: "available after once your Product is in process",
  },
});

const Order = mongoose.model("Order", orderSchema);

module.exports = Order;
