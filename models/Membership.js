const mongoose = require('mongoose');

const membershipSchema = new mongoose.Schema({
  businessName: String,
  salesTaxReseller: String,
  email: String,
  businessTaxID: String,
  phoneNumber: String,
  streetAddress1: String,
  streetAddress2: String,
  city: String,
  state: String,
  zipCode: String,
  country: String,
  businessType: [String],
  primaryBuyer1: String,
  primaryBuyer2: String,
  emailUpdates: Boolean,
  textUpdates: Boolean,
  businessID1: String, // Assuming you store file paths or URLs in the database
  businessID2: String,
  taxCertificate: String,
  approved : {type: Boolean, default: false},
});

const Membership = mongoose.model('Membership', membershipSchema);

module.exports = Membership;
