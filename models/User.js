const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    userName: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    // approved: { type: Boolean, default: false },
    businessName: { type: String, required: true },
    resellerTaxID: { type: String, required: true },
    businessTaxID: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    streetAddress1: { type: String, required: true },
    streetAddress2: { type: String },
    city: { type: String, required: true },
    state: { type: String, required: true },
    zip: { type: String, required: true },
    country: { type: String, required: true },
    primaryBuyer1: {
      firstName: { type: String, required: true },
    },
    primaryBuyer2: {
      firstName: { type: String, required: true  },
    },
    // File path for Tax Certificate PDF
    taxCertificate: { type: String }, 
    businessID1: { type: String }, // File path for Business ID Forms PDF
    businessID2: { type: String },
  
    isAdmin: { type: Boolean, default: false },
    status: { type: String, default: 'pending' }, 
  },
  { timestamps: true }
);

const User = mongoose.model("User", UserSchema);
module.exports = User;
